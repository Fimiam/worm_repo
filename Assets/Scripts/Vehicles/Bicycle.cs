using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bicycle : MonoBehaviour
{
    public Transform gearTransform, stearModel, frontWheelModel, backWheelMoodel;
    public Transform rightFootTarget, leftFootTarget, ringtHandTarget, leftHandTarget;

    public float MaxStearAngle, wheelsRotationMult = 155, gearRotationMult = 75;

    public void UpdateWheels(float speed)
    {
        frontWheelModel.Rotate(Vector3.right * speed * wheelsRotationMult * Time.deltaTime);
        backWheelMoodel.Rotate(Vector3.right * speed * wheelsRotationMult * Time.deltaTime);
    }

    public void UpdateStear(float stearAngle)
    {
        stearModel.localRotation = Quaternion.Euler(Vector3.up * stearAngle);
    }

    public void UpdateGear(float rotSpeed)
    {
        gearTransform.Rotate(Vector3.right * rotSpeed * gearRotationMult * Time.deltaTime);
    }
}
