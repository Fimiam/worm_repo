using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialController : GameplayController
{
    public override void Init()
    {
        if(Game.Instance.ProgressData.CurrentLevel == 0)
        {
            gameplay.GetController<UIController>().tutorialUI.controlTutorial.Show();
        }
    }
}
