using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class TutorControl : MonoBehaviour
{
    [SerializeField] private CanvasGroup group;

    [SerializeField] private float inputToComplete = 3;

    public void Show()
    {
        group.alpha = 0;
        gameObject.SetActive(true);
        group.DOFade(1, .2f);
    }

    private void Update()
    {
        inputToComplete -= Mathf.Abs(InputController.PlayerInput.x);

        if (inputToComplete < 0) Complete();
    }

    private void Complete()
    {
        enabled = false;

        group.DOFade(0, .3f).onComplete += () => gameObject.SetActive(false);
    }
}
