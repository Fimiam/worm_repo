using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Linq;

public class SkinsLists : MonoBehaviour
{
    [SerializeField] private RectTransform listsContainer;

    [SerializeField] private int defaultList = 0;

    [SerializeField] private float switchDelta = 400;

    [SerializeField] private float sens = 3;

    [SerializeField] private float minWidthIndicator = 50.0f, maxWidthIndicator = 200.0f;

    [SerializeField] private float indicatorSizingSpeed = 33f;

    [SerializeField] private List<RectTransform> listIndicators;

    [SerializeField] private List<CanvasGroup> nameGroups;

    public List<float> xOffsets = new List<float>();

    private Vector3 lastPos;

    private bool firstTap;

    private float treshold;

    int closest = 0, secondClosest = 1;

    void Start()
    {
        foreach (RectTransform item in listsContainer)
        {
            xOffsets.Add(item.anchoredPosition.x * -1f);
        }

        var pos = listsContainer.anchoredPosition;

        pos.x = xOffsets[closest];

        listsContainer.anchoredPosition = pos;

        nameGroups[closest].alpha = 1.0f;

        listsContainer.ForceUpdateRectTransforms();
    }

    void Update()
    {
        if(Input.GetMouseButton(0))
        {
            treshold += InputController.PlayerInput.x * sens;

            if(Mathf.Abs(treshold) > 60.0f && IsPointerOverUIObject())
            {
                var pos = listsContainer.anchoredPosition;

                pos.x += InputController.PlayerInput.x * sens;

                pos.x = Mathf.Clamp(pos.x, xOffsets[xOffsets.Count - 1], xOffsets[0]);

                listsContainer.anchoredPosition = pos;
            }
        }
        else
        {
            var pos = listsContainer.anchoredPosition;

            var targetX = xOffsets[closest];

            pos.x = Mathf.Lerp(pos.x, targetX, .06f);

            listsContainer.anchoredPosition = pos;
        }

        if(Input.GetMouseButtonUp(0))
        {
            treshold = 0.0f;
        }


        var distances = new List<float>();

        foreach (RectTransform item in listsContainer)
        {
            distances.Add(Mathf.Abs(transform.InverseTransformPoint(item.position).x));
        }

        float min, secondMin;

        min = secondMin = Mathf.Infinity;

        for (int i = 0; i < distances.Count; i++)
        {
            if(distances[i] < min)
            {
                secondClosest = closest;

                secondMin = distances[closest];

                min = distances[i];
                closest = i;
            }
            else if(distances[i] < secondMin && distances[i] != min)
            {
                secondMin = distances[i];
                secondClosest = i;
            }
        }

        if (closest == 0 && closest == secondClosest) secondClosest = 1;

        float width1 = 0, width2 = 0;

        var rate = distances[closest] / distances[secondClosest];

        var rate1 = 1.0f - rate * .5f;
        var rate2 = rate * .5f;

        width1 = Mathf.Lerp(minWidthIndicator, maxWidthIndicator, rate1);
        width2 = Mathf.Lerp(minWidthIndicator, maxWidthIndicator, rate * .5f);

        var rect1 = listIndicators[closest];
        var rect2 = listIndicators[secondClosest];

        var size = rect1.sizeDelta;

        size.x = width1;

        rect1.sizeDelta = size;

        size = rect2.sizeDelta;

        size.x = width2;

        rect2.sizeDelta = size;

        nameGroups[closest].alpha = rate1;
        nameGroups[secondClosest].alpha = rate2;
        

    }

    private bool IsPointerOverUIObject()
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Exists(r => r.gameObject == gameObject);
    }
}
