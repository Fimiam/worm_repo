﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class RateUsPopup : MonoBehaviour
{
    private const int STARS_TO_RATE = 4;

    [SerializeField] private TextMeshProUGUI title_Text;

    [SerializeField]
    private List<Button> stars;

    [SerializeField]
    private Button sumbit_Button, later_Button;

    private void Start()
    {
        title_Text.text = $"Enjoying {Application.productName} ?";

        stars.ForEach(s => s.onClick.AddListener (() => StarClicked(s.transform.GetSiblingIndex())));

        sumbit_Button.onClick.AddListener(SumbitClicked);
        later_Button.onClick.AddListener(LaterClicked);
    }

    public void Show()
    {
        CleanStars();
    }

    private void StarClicked(int _index)
    {
        CleanStars();

        for (int i = 0; i < _index + 1; i++)
        {
            stars[i].transform.GetChild(0).gameObject.SetActive(true);
        }
    }

    private void SumbitClicked()
    {
        int stars = this.stars.FindAll(s => s.transform.GetChild(0).gameObject.activeSelf).Count;

        if(stars < STARS_TO_RATE)
        {
            FakeRate();
        }
        else
        {
            Application.OpenURL("market://details?id=" + Application.productName);
        }

        Hide();
    }

    private void CleanStars()
    {
        stars.ForEach(s => s.transform.GetChild(0).gameObject.SetActive(false));
    }

    private void LaterClicked()
    {
        Hide();
    }

    private void Hide()
    {
       
    }

    private void FakeRate()
    {

    }

    private string MyEscapeURL(string url)
    {
        return WWW.EscapeURL(url).Replace("+", "%20");
    }
}
