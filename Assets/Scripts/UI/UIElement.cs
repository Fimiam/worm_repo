using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIElement : MonoBehaviour
{
    public virtual void Hide()
    {
        
    }

    public virtual void Show()
    {

    }
}
