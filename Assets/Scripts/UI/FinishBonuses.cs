using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishBonuses : MonoBehaviour
{
    [SerializeField] private List<FinishUIBonus> bonuses;

    public void ShowBonus(int bonus)
    {
        bonuses.ForEach(b => b.Hide());

        var toShow = bonuses.Find(b => b.bonus == bonus);

        if (toShow) toShow.Show();
    }
}
