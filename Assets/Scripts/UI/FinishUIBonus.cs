using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class FinishUIBonus : MonoBehaviour
{
    public int bonus = 1;

    [SerializeField] private RectTransform textRect;
    [SerializeField] private TextMeshProUGUI text;


    [SerializeField] private ParticleSystem particles;

    public void Show()
    {
        var rect = GetComponent<RectTransform>();

        rect.anchoredPosition = Random.insideUnitCircle * 200;

        textRect.localRotation = Quaternion.Euler(Vector3.forward * Random.Range(-20, 20));

        gameObject.SetActive(true);

        textRect.DOPunchScale(Vector3.one * .2f, .4f, 2);
        text.DOFade(1, .2f);
        if (particles) particles.Play();
    }

    public void Hide()
    {
        gameObject.SetActive(false);
    }
}
