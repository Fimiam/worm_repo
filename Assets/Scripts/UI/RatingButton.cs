using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RatingButton : UIButton
{
    protected override void Click()
    {
        base.Click();

        Application.OpenURL("market://details?id=" + Application.productName);
    }
}
