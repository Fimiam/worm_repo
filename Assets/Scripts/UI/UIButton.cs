using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButton : MonoBehaviour
{
    protected virtual void Start()
    {
        GetComponent<Button>().onClick.AddListener(Click);
    }

    protected virtual void Click()
    {
        Game.Instance.Vibrations.ButtonClickVibro();
    }
}
