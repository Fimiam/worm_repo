using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoAdsButton : UIButton
{
    protected override void Start()
    {
        base.Start();

        gameObject.SetActive(!Game.Instance.ProgressData.Premium);
    }

    protected override void Click()
    {
        base.Click();

        Purchaser.Instance.BuyPremium();
    }
}
