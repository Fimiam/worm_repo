using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinItemUI : MonoBehaviour
{
    public int skinID;

    [SerializeField] private Button selectButton, buyButton;

    [SerializeField] private GameObject selectionFrame;

    private UIController uiController;

    public void Setup(UIController uiController)
    {
        this.uiController = uiController;
    }

    private void Start()
    {
        selectButton.onClick.AddListener(SelectClick);
    }

    private void SelectClick()
    {
        uiController.SelectSkin(skinID);
    }

    public void SetSelected(bool selected)
    {
        selectionFrame.SetActive(selected);
    }
}
