using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : GameplayController
{
    [SerializeField] private TapToStart tapToStart;
    [SerializeField] private SidePanel sidePanel;

    public SkinsPanel skinsPanel;
    public TutorialUI tutorialUI;


    public override void StartGame()
    {
        tapToStart.Hide();
        sidePanel.Hide();
    }

    public void SelectSkin(int skinID)
    {
        skinsPanel.SelectSkin(skinID);
    }
}
