using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using DG.Tweening;

public class TutorialMaskPanel : MonoBehaviour
{
    public TutorialType type;

    public Transform trackingTransfom;

    [SerializeField] private Vector3 trackPositionOffset;

    [SerializeField] private float timeout, appearingDuration = .5f, disappearingDuration = .5f;

    [SerializeField] private RectTransform textRect;

    [SerializeField] private RectTransform rect;

    [SerializeField] private Canvas canvas;

    [SerializeField] private Image maskImage;

    [SerializeField] private CanvasGroup group;

    [SerializeField] private float sqrDistToComplete = .2f;

    [SerializeField] private AnimationCurve timescaleCurve, appearFillMaskCurve, appearAlphaCurve;

    private CaterpillarController caterpillarController;
    private MovementController movementController;

    public void ActivateMaskTutorial(Transform trackTransform)
    {
        trackingTransfom = trackTransform;

        enabled = true;

        StartCoroutine(Showing());
    }

    private void Update()
    {
        if(trackingTransfom == null)
        {
            enabled = false;

            return;
        }

        var sp = Camera.main.WorldToScreenPoint(trackingTransfom.position + trackPositionOffset);

        if (sp.y < 0 || sp.x < 0) return;
        if (sp.y > Screen.height || sp.x > Screen.width) return;

        var x = sp.x / Screen.width * canvas.GetComponent<RectTransform>().sizeDelta.x;
        var y = sp.y / Screen.height * canvas.GetComponent<RectTransform>().sizeDelta.y;

        rect.anchoredPosition = new Vector2(x, y);
    }

    private IEnumerator Showing()
    {
        textRect.DOPunchScale(Vector3.one * .2f, .4f, 2).SetLoops(-1);

        float t = 0.0f;

        float rate = default;

        while(t < appearingDuration)
        {
            t += Time.unscaledDeltaTime;

            rate = t / appearingDuration;

            movementController.timeScale = timescaleCurve.Evaluate(rate);

            maskImage.fillAmount = appearFillMaskCurve.Evaluate(rate);
            group.alpha = appearAlphaCurve.Evaluate(rate);

            yield return null;
        }

        float inputToComplete = 2;

        while(true)
        {
            if (Input.GetMouseButtonDown(0)) break;

            if ((trackingTransfom.position - caterpillarController.caterpillar.head.transform.position).sqrMagnitude < sqrDistToComplete) break;

            inputToComplete -= Mathf.Abs(InputController.PlayerInput.x);

            if (inputToComplete < 0) break;

            timeout -= Time.unscaledDeltaTime;

            if (timeout < 0) break;

            yield return null;
        }

        maskImage.fillClockwise = false;

        t = 0f;

        while (t < disappearingDuration)
        {
            t += Time.unscaledDeltaTime;

            rate = t / appearingDuration;

            movementController.timeScale = timescaleCurve.Evaluate(1.0f - rate);

            maskImage.fillAmount = appearFillMaskCurve.Evaluate(1.0f - rate);
            group.alpha = appearAlphaCurve.Evaluate(1.0f - rate);

            yield return null;
        }

        gameObject.SetActive(false);
    }
}

public enum TutorialType
{
    Food, Cubes, Obstacles
}
