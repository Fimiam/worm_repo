﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundButton : MonoBehaviour
{
    private const string KEY = "sound";

    [SerializeField] private Image image;

    [SerializeField] private Sprite on, off;

    void Start()
    {
        GetComponent<Button>().onClick.AddListener(OnClick);

        var active = Game.Instance.Persistance.GetData<SoundsData>().SoundActive;

        if(active)
        {
            image.sprite = on;
        }
        else
        {
            image.sprite = off;
        }
    }

    private void OnClick()
    {
        var active = Game.Instance.Persistance.GetData<SoundsData>().SoundActive;

        active = !active;

        if (active)
        {
            image.sprite = on;
        }
        else
        {
            image.sprite = off;
        }

        Game.Instance.Vibrations.ButtonClickVibro();

        Game.Instance.Persistance.GetData<SoundsData>().SetSound(active);
    }
}
