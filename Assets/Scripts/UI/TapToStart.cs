using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class TapToStart : UIElement
{
    [SerializeField] private new Animation animation;

    [SerializeField] private TextMeshProUGUI text;

    public override void Show()
    {
        gameObject.SetActive(true);
    }

    public override void Hide()
    {
        animation.enabled = false;

        transform.DOScale(0, .3f).onComplete += () => gameObject.SetActive(false);
        text.DOFade(0, .2f);
    }
}
