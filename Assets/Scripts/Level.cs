using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class Level : MonoBehaviour
{
    public SplineComputer spline;
    public Transform startPoint;
    public Finish finish;

    private void Start()
    {
        spline.RebuildImmediate();
    }

    public void SetFinish(Finish finishPrefab)
    {
        finish = Instantiate(finishPrefab);

        var sample = spline.Evaluate(1.0f);

        finish.transform.position = sample.position;
        finish.transform.forward = sample.forward;

        finish.transform.SetParent(transform);
    }
}
