using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class NumericCube : Hittable
{
    private int emissionProp;

    [SerializeField] private Gradient gamma_0_100;

    [Range(0, 100)]
    [SerializeField] private int numberMin, numberMax;

    [SerializeField] private new Renderer renderer;

    [SerializeField] private TextMeshPro text;

    [SerializeField] private Transform partsContainer, model;

    [SerializeField] private ParticleSystem hitCubeEffect_prefab;

    private List<Rigidbody> partsRbs = new List<Rigidbody>();

    private List<Renderer> partsRenderers = new List<Renderer>();

    private Material mat;
    
    void Start()
    {
        foreach (Transform item in partsContainer)
        {
            partsRbs.Add(item.GetComponent<Rigidbody>());
            partsRenderers.Add(item.GetComponent<Renderer>());
        }


        emissionProp = Shader.PropertyToID("_EmissionColor");

        health = Random.Range(numberMin, numberMax);

        mat = renderer.material;

        mat.color = gamma_0_100.Evaluate(health / 100.0f);
        mat.SetColor(emissionProp, mat.color * .4f);

        partsRenderers.ForEach(r => r.material = mat);

        text.text = $"{health}";
    }

    public override void GetHit(int val = 1)
    {
        base.GetHit(val);

        text.text = $"{health}";

        mat.color = gamma_0_100.Evaluate(health / 100.0f);
        mat.SetColor(emissionProp, mat.color * .4f);

        model.DOPunchPosition(Vector3.forward * .08f, .08f);

        var effect = Instantiate(hitCubeEffect_prefab);

        effect.transform.position = transform.position;
        effect.transform.rotation = transform.rotation;

        var eMat = effect.GetComponent<ParticleSystemRenderer>().material;

        eMat.color = mat.color;
        eMat.SetColor(emissionProp, mat.color * .4f);

        effect.Play();

        Destroy(effect.gameObject, 5);
    }

    public override void DestroyHitting()
    {
        partsContainer.gameObject.SetActive(true);

        partsContainer.parent = null;

        partsRenderers.ForEach(r => r.material = mat);

        for (int i = 0; i < partsContainer.childCount; i++)
        {
            partsRbs[i].isKinematic = false;

            var force = transform.forward * 3 + Vector3.up * 5 + Random.insideUnitSphere * 3;

            partsRbs[i].AddForce(force, ForceMode.Impulse);

            Game.Instance.StartCoroutine(Scaling(partsRbs[i].transform));
        }

        gameObject.SetActive(false);
    }

    private IEnumerator Scaling(Transform part)
    {
        yield return new WaitForSeconds(1.6f);

        part.DOScale(0, 2f);
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    var head = collision.collider.GetComponent<Head>();

    //    if (head) head.caterpillar.Crash();
    //}
}
