using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Dreamteck.Splines;

public class MovementController : GameplayController
{
    public SplineSample movementPoint;
    public Transform orientationPoint, cameraOrientationPoint;

    public Vector3 movePosition;
    public Vector3 moveNormal;

    [SerializeField] private SplineProjector projector;
    private SplineComputer spline;

    [SerializeField] private AnimationCurve cubeHitTimePunchCurve;

    private Finish finish;

    private float pathLength, currentDistance;

    private bool movement;

    public float trackProgress => Mathf.InverseLerp(beginTrackProgress, 1, (float)movementPoint.percent);

    private float beginTrackProgress;

    private float cameraPointDistance;

    public float timeScale = 1.0f, punchTime;

    private bool finished;

    private CaterpillarController caterpillarController;

    private LevelController levelController;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        caterpillarController = gameplay.GetController<CaterpillarController>();
        levelController = gameplay.GetController<LevelController>();
    }

    public override void LevelLoaded()
    {
        spline = levelController.level.spline;

        orientationPoint.position = levelController.level.startPoint.position;
        orientationPoint.rotation = levelController.level.startPoint.rotation;

        pathLength = spline.CalculateLength();

        projector.spline = spline;
        projector.projectTarget = orientationPoint;

        projector.CalculateProjection();

        beginTrackProgress = (float)projector.result.percent;

        currentDistance = beginTrackProgress * pathLength;

        movementPoint = projector.result;

        cameraOrientationPoint.position = orientationPoint.position = movementPoint.position;

        cameraOrientationPoint.rotation = orientationPoint.rotation = movementPoint.rotation;

        caterpillarController.SpawnCaterpillar();
    }


    public override void CaterpillarSpawned()
    {
        projector.projectTarget = caterpillarController.caterpillar.head.transform;

        Debug.Log("spawned");
    }

    private void Update()
    {
        if (!movement && Input.GetMouseButtonDown(0) && !InputController.IsPointerOverUI)
        {
            movement = true;

            caterpillarController.caterpillar.BeginMove();

            gameplay.StartGame();
        }

        Time.timeScale = timeScale;

        Time.fixedDeltaTime = timeScale / 1.0f * .02f;

        if (!movement) return;

        if(!finished)
        {
            CommonMovement();
        }
        else
        {
            FinishMovement();
        }

    }

    private void CommonMovement()
    {
        projector.CalculateProjection();

        currentDistance = (float)projector.result.percent * pathLength;

        movementPoint = projector.result;

        orientationPoint.position = movementPoint.position;

        orientationPoint.rotation = movementPoint.rotation;

        if (currentDistance >= cameraPointDistance)
        {
            cameraPointDistance = currentDistance;
            cameraOrientationPoint.SetPositionAndRotation(orientationPoint.position, orientationPoint.rotation);
        }

        if (!finished && projector.result.percent > .999f)
        {
            cameraPointDistance = 0.0f;

            finished = true;

            Debug.Log("Finish");

            projector.spline = finish.spline;

            projector.RebuildImmediate();

            pathLength = finish.spline.CalculateLength();
        }

        Debug.Log("commonMovement");
    }

    private void FinishMovement()
    {

    }

    public void PlayerCrashedCube()
    {
        StartCoroutine(TimePunch());
    }

    private IEnumerator TimePunch()
    {
        punchTime = 0.0f;

        var duration = cubeHitTimePunchCurve.keys[cubeHitTimePunchCurve.keys.Length - 1].time; 

        while(punchTime < duration)
        {
            punchTime += Time.unscaledDeltaTime;

            timeScale = cubeHitTimePunchCurve.Evaluate(punchTime);

            yield return null;
        }

        timeScale = 1.0f;
    }
}
