using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Segment : MonoBehaviour
{
    public bool isHead;

    public Caterpillar caterpillar;

    public Transform model;

    public Vector3 offset;

    public float minDist;

    public SkinCollider skinCollider;

    public ParticleSystem cutEffect_prefab;

    public virtual void SnakeBehaviour(Caterpillar caterpillar)
    {
        var dist = (caterpillar.headSegment.transform.position - caterpillar.positions[0]).magnitude;

        Vector3 dir = default;

        if (dist > caterpillar.headSegment.minDist)
        {
            dir = (caterpillar.head.transform.position - caterpillar.positions[0]).normalized;

            caterpillar.positions.Insert(0, caterpillar.positions[0] + dir * caterpillar.headSegment.minDist);
            caterpillar.positions.RemoveAt(caterpillar.positions.Count - 1);
            caterpillar.modelForwards.Insert(0, caterpillar.headSegment.model.forward);
            caterpillar.modelForwards.RemoveAt(caterpillar.modelForwards.Count - 1);

            dist -= caterpillar.headSegment.minDist;
        }

        Vector3 targetPos = default;

        Quaternion targetRotation = default;

        for (int i = 1; i < caterpillar.segments.Count; i++)
        {
            targetPos = Vector3.Lerp(caterpillar.positions[i], caterpillar.positions[i - 1], dist / caterpillar.headSegment.minDist);

            caterpillar.segments[i].transform.position = Vector3.Lerp(caterpillar.segments[i].transform.position, targetPos, 1.0f);

            dir = caterpillar.segments[i - 1].transform.position - caterpillar.segments[i].transform.position;

            if (Mathf.Approximately(dir.sqrMagnitude, 0.0f)) dir = caterpillar.headSegment.transform.forward;

            //Debug.Log(dir);

            //targetRotation = Quaternion.LookRotation(Vector3.Lerp(caterpillar.modelForwards[i], caterpillar.modelForwards[i - 1], dist / caterpillar.headSegment.minDist));

            //caterpillar.segments[i].model.rotation = Quaternion.LookRotation(dir); // car cool
            caterpillar.segments[i].transform.rotation = Quaternion.LookRotation(dir);// cool default

            //segments[i].transform.rotation = Quaternion.Lerp(segments[i].transform.rotation, Quaternion.LookRotation(dir), 1.0f);
        }
    }

    public void Cut(bool effect = true)
    {
        if(effect && cutEffect_prefab != null)
        {
            var e = Instantiate(cutEffect_prefab);

            e.transform.position = transform.position;
        }

        skinCollider.collider.enabled = false;

        gameObject.SetActive(false);
    }
}
