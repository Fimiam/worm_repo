using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class Food : MonoBehaviour
{
    public int amount = 1;

    [SerializeField] private Transform skinContainer;

    [SerializeField] private TextMeshPro amountText;

    public FoodSkin skin;

    private new Camera camera;

    private void Start()
    {
        var prefab = Game.Instance.Configurations.skinConfigs.skins[Game.Instance.ProgressData.CurrentSkin].foodSkin;

        if (skin) Destroy(skin.gameObject);

        skin = Instantiate(prefab);

        skin.transform.SetParent(skinContainer);

        skin.transform.localPosition = prefab.transform.localPosition;
        skin.transform.localScale = prefab.transform.localScale;

        amountText.text = $"+ {amount}";

        camera = Camera.main;
    }

    private void Update()
    {
        var dir = amountText.transform.position - camera.transform.position;

        amountText.transform.forward = dir.normalized;
    }

    private void OnTriggerEnter(Collider other)
    {
        var head = other.GetComponent<Head>();

        if (head)
        {
            head.caterpillar.AddSegment(amount);

            skin.FoodEaten();

            var seq = DOTween.Sequence();

            seq.Append(amountText.transform.DOMove(amountText.transform.position + amountText.transform.up, .4f));
            seq.Join(amountText.DOFade(0, .35f));

            seq.onComplete += () => gameObject.SetActive(false);

            GetComponent<Collider>().enabled = false;

            //gameObject.SetActive(false);
        }
    }

    public void SetSkin(FoodSkin foodSkin_prefab)
    {
        if (skin) Destroy(skin.gameObject);

        skin = Instantiate(foodSkin_prefab);

        skin.transform.SetParent(skinContainer);

        skin.transform.localPosition = foodSkin_prefab.transform.localPosition;
        skin.transform.localScale = foodSkin_prefab.transform.localScale;
    }
}
