using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : GameplayController
{
    [HideInInspector] public Level level;

    [SerializeField] private Transform levelContainer;

    private UIController uiController;


    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        uiController = gameplay.GetController<UIController>();
    }

    public override void Init()
    {
        StartCoroutine(LoadLevel());
    }

    private IEnumerator LoadLevel()
    {
        level = Instantiate(gameplay.Level.levelPrefab);

        level.transform.SetParent(levelContainer);

        level.transform.localPosition = Vector3.zero;

        var finish = Game.Instance.Configurations.skinConfigs.skins[Game.Instance.ProgressData.CurrentSkin].finish;

        level.SetFinish(finish);

        yield return null;

        gameplay.LevelLoaded();
    }

    public override void CaterpillarSpawned()
    {
        base.CaterpillarSpawned();

        var finish = Game.Instance.Configurations.skinConfigs.skins[Game.Instance.ProgressData.CurrentSkin].finish;

        level.SetFinish(finish);
    }

    public void SetFinishBonus(int lastBonus)
    {
        ///uiController...
    }
}
