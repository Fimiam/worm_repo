using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Caterpillar : MonoBehaviour
{
    public int initialSegmentsAdded = 3;

    public Head head;

    public LayerMask sideMoveCheckMask;

    [SerializeField] private AnimationCurve eatingTimeCurve, eatingPunchCurve;

    public List<Vector3> positions, modelForwards;

    private MovementController movementController;

    public float speed, sideSpeed = 1;
    [SerializeField] private float newFoodInterval;


    public float safeSideDist = .01f, rotationTrashold = .01f;

    [Range(0, 1.0f)]
    public float crashRate = .1f;

    [Header("Rotation")]

    [Header("curve x - currentLookAngle, y - rotationSpeed")]
    public AnimationCurve lookRotationCurve;

    [Header("curve x - currentLookAngle, y - stableSpeed")]
    public AnimationCurve lookRotationStableCurve;

    [Range(0.0f, 1.0f)]
    [SerializeField] private float differentSideRotationDamp = .3f;

    private List<Segment> segment_prefabs;

    public List<Segment> segments = new List<Segment>();

    private int spawnedSegment;

    private Vector3 lastPosition;

    private float skinHeight = .2f;

    private List<int> foodQueue = new List<int>();

    private float lastFoodTime;

    private Rigidbody rb;

    public bool crashState;

    public List<Hittable> hittable = new List<Hittable>();

    private Segment tailSegment_prefab;

    public Segment headSegment => segments[0];

    private CaterpillarMovement movement;

    public bool noControl;

    public void Setup(MovementController movementController)
    {
        this.movementController = movementController;

        movement = new CaterpillarMovement();

        movement.movementController = movementController;
        movement.caterpillar = this;
    }

    public void Spawn()
    {
        SetSkin(Game.Instance.ProgressData.CurrentSkin);

        lastPosition = head.transform.position = movementController.orientationPoint.position +
            (movementController.orientationPoint.up * ((movementController.movementPoint.size + skinHeight) * .5f));

        head.transform.rotation = Quaternion.LookRotation(movementController.orientationPoint.forward, movementController.orientationPoint.up);

        positions.Add(head.transform.position);
        modelForwards.Add(headSegment.model.forward);

        headSegment.transform.position = head.transform.position;// + head.rotation * headSegment.offset;
        headSegment.transform.rotation = head.transform.rotation;

        headSegment.caterpillar = this;

        enabled = false;
    }

    public void SetSkin(int id)
    {
        var headPrefab = Game.Instance.Configurations.skinConfigs.skins[id].headSegment;

        tailSegment_prefab = Game.Instance.Configurations.skinConfigs.skins[id].tailSegment;

        segment_prefabs = Game.Instance.Configurations.skinConfigs.skins[id].snakeSegments;


        if (segments.Count > 0)
        {
            var current = headSegment;

            Destroy(current.gameObject);

            segments.Clear();

            var newHead = Instantiate(headPrefab);

            newHead.transform.SetParent(transform);

            newHead.transform.localScale = headPrefab.transform.localScale;

            segments.Add(newHead);


            var targetPos = head.transform.position;// + head.rotation * headSegment.offset;

            headSegment.transform.position = targetPos;
            headSegment.transform.rotation = head.transform.rotation;

            headSegment.caterpillar = this;

            enabled = false;
        }
        else
        {
            var newHead = Instantiate(headPrefab);

            newHead.transform.SetParent(transform);

            newHead.transform.localScale = headPrefab.transform.localScale;

            segments.Add(newHead);
        }
    }

    public void BeginMove()
    {
        if(initialSegmentsAdded > 0)
            AddSegment(initialSegmentsAdded, false);

        enabled = true;
    }
    
    void Update()
    {
        if (segments.Count < 1) return;

        movement.Move();

        if(Input.GetKeyDown(KeyCode.A))
        {
            AddSegment();
        }
    }

    public void Crash(int damageValue)
    {
        if(segments.Count < 2)
        {
            Dead();

            gameObject.SetActive(false);

            return;
        }

        var crashShift = -movementController.orientationPoint.forward * .3f;

        for (int i = 0; i < damageValue && segments.Count > 1; i++)
        {
            var toRemove = segments[1];

            toRemove.gameObject.SetActive(false);

            segments.RemoveAt(1);

            positions.RemoveAt(positions.Count - 1);
        }


        for (int i = 0; i < positions.Count; i++)
        {
            positions[i] += crashShift;
        }

        for (int i = 0; i < segments.Count; i++)
        {
            segments[i].transform.position += crashShift;
        }


        Snake();
    }

    public void CutFromSegment(Segment segment)
    {
        if (!segments.Contains(segment)) return;

        //Debug.Log("1");

        var index = segments.IndexOf(segment);

        if(index == 0)
        {
            Dead();
        }

        var toCut = new List<Segment>();

        var remove = segments.Count;

        //Debug.Log("2");

        for (int i = index; i < remove; i++)
        {
            toCut.Add(segments[index]);
            segments.RemoveAt(index);
            positions.RemoveAt(index);
            modelForwards.RemoveAt(index);
        }

        //Debug.Log("3");

        StartCoroutine(CuttingAlong(toCut));

        //Debug.Log("4");
    }

    private IEnumerator CuttingAlong(List<Segment> toCut)
    {
        float rate = .04f;

        for (int i = 0; i < toCut.Count; i++)
        {
            toCut[i].Cut(i == 0 || Random.value > .3f);

            yield return new WaitForSeconds(rate);
        }
    }

    private void Dead()
    {
        enabled = false;

        Debug.Log("GAMEOVER");
    }

    public void Snake()
    {
        headSegment.SnakeBehaviour(this);
    }

    public void AddSegment(int amount = 1, bool food = true, bool punch = false)
    {
        if(!food)
        {
            for (int i = 0; i < amount; i++)
            {
                var segment = Instantiate(segment_prefabs[spawnedSegment]);

                segment.transform.SetParent(transform);
                segment.transform.position = segments[segments.Count - 1].transform.position;

                segment.caterpillar = this;

                segments.Add(segment);
                positions.Add(segment.transform.position);
                modelForwards.Add(headSegment.model.forward);

                if(punch)
                    segment.transform.DOPunchScale(Vector3.one * eatingPunchCurve.Evaluate(1), eatingTimeCurve.Evaluate(1), 1, 1);

                spawnedSegment++;

                if (spawnedSegment > segment_prefabs.Count - 1) spawnedSegment = 0;
            }
        }
        else
        {

            if(foodQueue.Count > 0)
            {
                if(Time.realtimeSinceStartup - newFoodInterval > lastFoodTime)
                {
                    foodQueue.Add(amount);
                    StartCoroutine(Eating());
                }
                else
                {
                    foodQueue[foodQueue.Count - 1] += amount;
                }    
            }
            else
            {
                foodQueue.Add(amount);
                StartCoroutine(Eating());
            }
        }
    }

    private IEnumerator Eating()
    {
        lastFoodTime = Time.realtimeSinceStartup;

        var amount = foodQueue[foodQueue.Count - 1];

        foodQueue.RemoveAt(foodQueue.Count - 1);

        float dur = .16f;

        for (int i = 0; i < segments.Count; i++)
        {
            dur = eatingTimeCurve.Evaluate(i / (float)segments.Count);

            segments[i].transform.DOPunchScale(Vector3.one * eatingPunchCurve.Evaluate(i / (float)segments.Count), dur, 1, 1);

            yield return new WaitForSeconds(dur * .8f);
        }

        AddSegment(amount, false, true);
    }

    public void HeadCollisionEnter(Collision collision)
    {
        var hittable = collision.collider.GetComponent<Hittable>();

        if(hittable)
        {
            AddHiting(hittable);
        }

        if (!crashState)
        {
            var c = collision.contacts[0];

            var contactDir = head.transform.position - c.point;

            var distance = contactDir.magnitude;

            var dot = Vector3.Dot(head.transform.right, contactDir.normalized);

            var shift = (Mathf.Sign(dot) * head.transform.right) * (1.0f - distance / (head.collider.radius + safeSideDist)) * head.collider.radius;

            shift.y = 0;

            head.transform.position += shift;
        }
    }

    public void HeadCollisionStay(Collision collision)
    {
        if (!crashState)
        {
            var c = collision.contacts[0];

            var contactDir = head.transform.position - c.point;

            var distance = contactDir.magnitude;

            var dot = Vector3.Dot(head.transform.right, contactDir.normalized);

            var shift = (Mathf.Sign(dot) * head.transform.right) * (1.0f - distance / (head.collider.radius + safeSideDist)) * head.collider.radius;

            shift.y = 0;

            head.transform.position += shift;
        }
    }

    public void HeadCollisionExit(Collision collision)
    {
        var hittable = collision.collider.GetComponent<Hittable>();

        RemoveHitting(hittable);
    }

    private void AddHiting(Hittable hittable)
    {
        if (this.hittable.Contains(hittable)) return;

        this.hittable.Add(hittable);

        crashState = true;
    }

    public void RemoveHitting(Hittable hittable)
    {
        this.hittable.Remove(hittable);

        crashState = this.hittable.Count > 0;

        if(hittable is NumericCube)
            movementController.PlayerCrashedCube();
    }
}

[System.Serializable]
public struct SnakePoint
{
    public Vector3 position;
    public Quaternion rotation;

    public SnakePoint(Vector3 pos, Quaternion rot)
    {
        position = pos;
        rotation = rot;
    }
}

