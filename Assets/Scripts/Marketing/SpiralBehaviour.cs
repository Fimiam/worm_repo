using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiralBehaviour : MonoBehaviour
{
    [SerializeField] private float speed = 3;

    [SerializeField] private SplineComputer spline;

    private Vector3 targetPosition, targetForward;

    private Head head;

    private float distance, length;

    private void OnTriggerEnter(Collider other)
    {
        length = spline.CalculateLength();

        head = other.GetComponent<Head>();

        if(head)
        {
            head.caterpillar.noControl = true;

            enabled = true;
        }
    }

    private void Update()
    {
        if (distance > length) return;

        var point = spline.Evaluate(distance / length);

        targetPosition = point.position;
        targetForward = point.forward;

        head.transform.position = Vector3.Lerp(head.transform.position, targetPosition, .1f);
        head.transform.forward = Vector3.Lerp(head.transform.forward, targetForward, .1f);

        distance += Time.deltaTime * speed;
    }
}
