using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;

    [Header("Android")]
    //[SerializeField] private string appID_Android = "";
    [SerializeField] private string bannerID_Android = "";
    [SerializeField] private string interstitialID_Android = "";
    [SerializeField] private string rewardedID_Android = "";

    [Header("IOS")]
    //[SerializeField] private string appID_IOS = "";
    [SerializeField] private string bannerID_IOS = "";
    [SerializeField] private string interstitialID_IOS = "";
    [SerializeField] private string rewardedID_IOS = "";

    private bool bannerShown, bannerLoaded;

    public InterstitialUnit interstitial;
    public RewardedUnit rewarded;
    public BannerUnit banner;

    private void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        MaxSdkCallbacks.OnSdkInitializedEvent += (MaxSdkBase.SdkConfiguration sdkConfiguration) => {
            // AppLovin SDK is initialized, start loading ads

            banner = new BannerUnit(bannerID_Android);
            interstitial = new InterstitialUnit(interstitialID_Android);
            rewarded = new RewardedUnit(rewardedID_Android);

            Debug.Log("max inited");
        };

        MaxSdk.SetSdkKey("q935SJXX1cAOysNKZAuRNwrvWPfE3UXuuQttZsADgQbsl79ioJfSBC0vF_fp6M-ixUShkHi7gKPLU9UOh1HWu4");
        MaxSdk.InitializeSdk();
    }

    public void DisableAds()
    {
        Debug.Log("disabling ads");

        PlayerPrefs.SetInt("ads_active", 0);

        ///Game.Instance.ProgressData.BoughtNoAds();

        banner.Hide();

        //var noAdsButton = FindObjectOfType<NoAdsButton>();

        //if (noAdsButton) noAdsButton.gameObject.SetActive(false);
    }
}
