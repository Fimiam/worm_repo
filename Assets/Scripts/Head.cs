using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Head : MonoBehaviour
{
    public Caterpillar caterpillar;

    public new SphereCollider collider;

    public new Rigidbody rigidbody;

    private void OnCollisionEnter(Collision collision)
    {
        caterpillar.HeadCollisionEnter(collision);

    }

    private void OnCollisionStay(Collision collision)
    {
        caterpillar.HeadCollisionStay(collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        caterpillar.HeadCollisionExit(collision);
    }
}
