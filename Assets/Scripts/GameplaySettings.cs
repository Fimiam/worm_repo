using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameplaySettings : MonoBehaviour
{
    [SerializeField] private Button settingsButton;

    [SerializeField] private new Animation animation;

    private bool shown = false;

    void Start()
    {
        settingsButton.onClick.AddListener(Click);
    }

    public void Click()
    {
        if(shown)
        {
            animation.Play("settingsClose");
        }
        else
        {
            animation.Play("settingsOpen");
        }


        shown = !shown;
    }

    public void Show()
    {
        animation.Play("settingsShow");
    }

    public void Hide()
    {
        animation.Play("settingsHide");
    }
}
