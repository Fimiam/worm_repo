using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeWings : MonoBehaviour
{
    [SerializeField] private List<Transform> wings;

    [SerializeField] private float swingRate;

    [SerializeField] private float downWingAngle, topWingAngle;

    [SerializeField] private int swingSteps;

    private int step;

    private bool down;

    private float toNextSwing, toNextStep;

    void Start()
    {
        StartCoroutine(Swinging());
    }

    private IEnumerator Swinging()
    {
        while(true)
        {


            for (int i = 0; i < swingSteps; i++)
            {
                toNextStep -= Time.deltaTime;

                if(toNextStep < 0)
                {
                    toNextStep = swingRate / (float)swingSteps;

                    if (down)
                    {
                        var angle = Mathf.Lerp(downWingAngle, topWingAngle, i / (float)swingSteps);

                        wings[0].localRotation = Quaternion.Euler(Vector3.right * angle);
                        wings[1].localRotation = Quaternion.Euler(Vector3.right * angle * -1.0f);
                    }
                    else
                    {
                        var angle = Mathf.Lerp(downWingAngle, topWingAngle, 1.0f - i / (float)swingSteps);

                        wings[0].localRotation = Quaternion.Euler(Vector3.right * angle);
                        wings[1].localRotation = Quaternion.Euler(Vector3.right * angle * -1.0f);
                    }
                }

                yield return null;
            }

            down = !down;

            //toNextSwing -= Time.deltaTime;

            //if(toNextSwing < 0.0f)
            //{
            //    down = !down;

            //    if(down)
            //    {
            //        wings[0].localRotation = Quaternion.Euler(Vector3.right * downWingAngle);
            //        wings[1].localRotation = Quaternion.Euler(Vector3.right * downWingAngle * -1.0f);
            //    }
            //    else
            //    {
            //        wings[0].localRotation = Quaternion.Euler(Vector3.right * topWingAngle);
            //        wings[1].localRotation = Quaternion.Euler(Vector3.right * topWingAngle * -1.0f);
            //    }

            //    toNextSwing = swingRate;
            //}

            //yield return null;
        }
    }
}
