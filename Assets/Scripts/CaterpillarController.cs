using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaterpillarController : GameplayController
{
    public Caterpillar caterpillar;

    private LevelController levelController;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        levelController = gameplay.GetController<LevelController>();

        caterpillar.Setup(gameplay.GetController<MovementController>());
    }

    public void SpawnCaterpillar()
    {
        caterpillar.Spawn();

        gameplay.CaterpillarSpawned();
    }
}
