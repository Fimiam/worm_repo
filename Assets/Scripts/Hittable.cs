using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hittable : MonoBehaviour
{
    [Range(-1, -.1f)]
    public float maximumFrontHitOffset = -.37f;

    public int health;

    public int damageValue = 1;

    public bool destroyed => health < 1;

    public virtual void GetHit(int val = 1)
    {
        if (destroyed) return;

        health -= val;

        if(health < 1)
        {
            DestroyHitting();
        }
    }

    public virtual void DestroyHitting()
    {

    }
}
