using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishSegment : MonoBehaviour
{
    public int bonusValue = 1;

    [SerializeField] private Finish finish;

    private void OnTriggerEnter(Collider other)
    {
        var head = other.GetComponent<Head>();

        if(head)
        {
            finish.SetBonus(bonusValue);
        }
    }
}
