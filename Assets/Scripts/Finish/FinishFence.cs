using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FinishFence : Hittable
{
    [SerializeField] private List<Collider> colliders;
    [SerializeField] private List<Rigidbody> rbs;
    [SerializeField] private new Collider collider;

    public override void DestroyHitting()
    {
        collider.enabled = false;

        var dir = transform.right;

        for (int i = 0; i < colliders.Count; i++)
        {
            var randomDir = Random.insideUnitSphere * 2;

            randomDir.y = 1.4f;

            colliders[i].enabled = true;
            rbs[i].isKinematic = false;
            rbs[i].AddForce(dir * 2.5f + randomDir, ForceMode.Impulse);

            if (i % 2 == 0) rbs[i].AddTorque(Random.insideUnitSphere * 180);
        }
    }
}
