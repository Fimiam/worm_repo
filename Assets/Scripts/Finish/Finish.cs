using Dreamteck.Splines;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Finish : MonoBehaviour
{
    public SplineComputer spline;

    public int lastBonus;

    private LevelController levelController;

    public void Setup(LevelController levelController)
    {
        this.levelController = levelController;
    }

    public void SetBonus(int bonusX)
    {
        lastBonus = bonusX;

        levelController.SetFinishBonus(lastBonus);

    }
}
