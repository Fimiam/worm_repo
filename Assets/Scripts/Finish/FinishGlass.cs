using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class FinishGlass : Hittable
{
    [SerializeField] private Transform partsParent;

    [SerializeField] private List<Rigidbody> rbs;

    [SerializeField] private new Collider collider;

    [SerializeField] private Renderer render;

    public override void DestroyHitting()
    {
        collider.enabled = false;

        partsParent.gameObject.SetActive(true);

        render.enabled = false;

        partsParent.SetParent(null);

        Vector3 dir = partsParent.forward * 4;

        foreach (var item in rbs)
        {
            item.isKinematic = false;

            item.AddForce(dir * Random.value + Random.insideUnitSphere * 4, ForceMode.Impulse);

            item.AddTorque(Random.insideUnitSphere * 20, ForceMode.Impulse);
        }

        List<Transform> trs = new List<Transform>();

        for (int i = 0; i < rbs.Count; i++)
        {
            trs.Add(rbs[i].transform);
        }

        StartCoroutine(Rotations(trs));
        StartCoroutine(Fading(trs));
    }

    private IEnumerator Rotations(List<Transform> transforms)
    {
        List<Vector3> rots = new List<Vector3>();

        for (int i = 0; i < rbs.Count; i++)
        {
            rots.Add(Random.insideUnitSphere * 360);
        }

        float t = 3;

        while(t < 0)
        {
            t -= Time.deltaTime;

            for (int i = 0; i < rots.Count; i++)
            {
                transforms[i].Rotate(rots[i]);
            }

            yield return null;
        }

    }

    private IEnumerator Fading(List<Transform> transforms)
    {
        yield return new WaitForSeconds(1.1f);

        float t = 1.6f;

        while(t > 0)
        {
            t -= Time.deltaTime;

            for (int i = 0; i < transforms.Count; i++)
            {
                transforms[i].localScale = Vector3.Lerp(transforms[i].localScale, Vector3.zero, .1f);
            }

            yield return null;
        }

        Destroy(partsParent.gameObject);
        //gameObject.SetActive(false);

    }
}
