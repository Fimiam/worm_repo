using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinsPanel : MonoBehaviour
{
    [SerializeField] private new Animation animation;

    [SerializeField] private List<SkinItemUI> skinsItems;

    [SerializeField] private RectTransform panel;

    private void Start()
    {
        skinsItems.ForEach(i => i.SetSelected(i.skinID == Game.Instance.ProgressData.CurrentSkin));
    }

    public void Show()
    {
        panel.gameObject.SetActive(true);

        animation.Play("skinsPanelShow");
    }

    public void Hide()
    {
        animation.Play("skinsPanelHide");
    }

    public void SelectSkin(int id)
    {
        var unlocked = Game.Instance.ProgressData.unlockedSkins.Contains(id);

        if (!unlocked) return;

        Game.Instance.ProgressData.SetSkin(id);

        skinsItems.ForEach(i => i.SetSelected(i.skinID == id));
    }
}
