using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialTrigger : MonoBehaviour
{
    [SerializeField] private TutorialType type;

    private void OnTriggerEnter(Collider other)
    {
        var head = other.GetComponent<Head>();

        if(head)
        {
            var tutors = FindObjectsOfType<TutorialMaskPanel>();

            foreach (var item in tutors)
            {
                if (item.type == type) item.ActivateMaskTutorial(transform);
            }
        }
    }
}
