using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LevelProgress : MonoBehaviour
{
    [SerializeField] private Image currentLevelImage, nextLevelImage, progressImage;

    [SerializeField] private TextMeshProUGUI currentLevelText, nextLevelText;


    void Start()
    {
        currentLevelText.text = $"{Game.Instance.ProgressData.ProgressionLevel}";
        nextLevelText.text = $"{Game.Instance.ProgressData.ProgressionLevel + 1}";
    }

    public void SetProgress(float progress)
    {
        progressImage.fillAmount = progress;
    }
}
