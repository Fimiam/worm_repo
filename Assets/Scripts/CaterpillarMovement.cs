using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaterpillarMovement
{
    public MovementController movementController;

    public Caterpillar caterpillar;

    private float lastXOffset;

    public virtual void Move()
    {
        if (!caterpillar.crashState && !caterpillar.noControl)
            caterpillar.head.transform.position += movementController.orientationPoint.forward * caterpillar.speed * Time.smoothDeltaTime;

        if (caterpillar.crashState && (caterpillar.headSegment.transform.position - caterpillar.head.transform.position).sqrMagnitude < .002f)
        {
            var localDist = caterpillar.hittable[0].transform.InverseTransformPoint(caterpillar.head.transform.position);

            var offset = localDist.z - caterpillar.head.collider.radius;

            if (offset < caterpillar.hittable[0].maximumFrontHitOffset)
            {
                if (caterpillar.hittable[0].health > 0)
                    caterpillar.Crash(caterpillar.hittable[0].damageValue);

                caterpillar.hittable[0].GetHit();


                if (caterpillar.hittable[0].health < 1)
                {
                    caterpillar.RemoveHitting(caterpillar.hittable[0]);
                }
            }
            else
            {
                caterpillar.hittable.RemoveAt(0);

                caterpillar.crashState = false;
            }
        }

        var sideMove = caterpillar.sideSpeed * InputController.PlayerInput.x;

        var moveDistAbs = Mathf.Abs(sideMove);

        var right = Mathf.Sign(sideMove) > 0;

        bool hasSideInput = moveDistAbs > 0.00f;

        if (hasSideInput)
        {
            var ray = new Ray(caterpillar.head.transform.position, right ? caterpillar.head.transform.right : -caterpillar.head.transform.right);

            if (Physics.Raycast(ray, out RaycastHit hit, Mathf.Infinity, caterpillar.sideMoveCheckMask))
            {
                var safeDist = hit.distance - (caterpillar.head.collider.radius + caterpillar.safeSideDist);

                if (moveDistAbs > safeDist)
                {
                    sideMove = safeDist * (right ? 1.0f : -1.0f);
                }
            }
        }

        if (!caterpillar.noControl)
            caterpillar.head.transform.position += movementController.orientationPoint.right * sideMove;

        var relatedPos = movementController.orientationPoint.InverseTransformPoint(caterpillar.head.transform.position);

        relatedPos.x = Mathf.Clamp(relatedPos.x, -1.4f + caterpillar.head.collider.radius + caterpillar.safeSideDist, 1.4f - caterpillar.head.collider.radius - caterpillar.safeSideDist);

        var xOffsetDelta = relatedPos.x - lastXOffset;

        var snakeCalculations = Mathf.CeilToInt(Mathf.Abs(xOffsetDelta) / caterpillar.headSegment.minDist) + 1;

        lastXOffset = relatedPos.x;

        caterpillar.head.transform.position = movementController.orientationPoint.TransformPoint(relatedPos);

        caterpillar.head.transform.rotation = movementController.orientationPoint.rotation;

        var targetPos = caterpillar.head.transform.position;// + head.rotation * headSegment.offset;

        caterpillar.headSegment.transform.position = Vector3.Lerp(caterpillar.headSegment.transform.position, targetPos, caterpillar.crashState ? caterpillar.crashRate : 1.0f);
        caterpillar.headSegment.transform.rotation = caterpillar.head.transform.rotation;

        Rotation(xOffsetDelta);

        for (int i = 0; i < snakeCalculations; i++)
            caterpillar.Snake();
    }

    private void Rotation(float xDelta)
    {
        var angle = caterpillar.segments[0].model.localEulerAngles.y;

        angle = ((angle + 180.0f) % 360.0f) - 180.0f;

        float speed = default;

        if (Mathf.Abs(xDelta) > caterpillar.rotationTrashold)
        {
            var inputMult = 1.0f;

            if ((angle < 0f && xDelta > 0f) || (angle > 0f && xDelta < 0f))
            {
                inputMult = 2.0f;
            }

            speed = caterpillar.lookRotationCurve.Evaluate(Mathf.Abs(angle)) * inputMult;

            angle += speed * xDelta;
        }
        else
        {
            speed = caterpillar.lookRotationStableCurve.Evaluate(Mathf.Abs(angle));

            angle = Mathf.MoveTowardsAngle(angle, 0, speed * Time.deltaTime);
        }

        caterpillar.headSegment.model.localEulerAngles = Vector3.up * angle;
    }
}
