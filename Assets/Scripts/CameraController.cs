using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : GameplayController
{
    [SerializeField] private Vector3 movePosOffset, moveLookOffset;

    [SerializeField] private float movePosSmooth, moveLookSmooth, skinPosSmooth, skinLookSmooth;

    [SerializeField] private float positionSmooth, lookAtSmooth;

    [SerializeField] private Transform container;

    [SerializeField] private new Camera camera;

    [SerializeField] private Vector3 crashOffset, skinPositionOffset, skinLookOffset;

    public Vector3 currentPositionOffset, currentLookAtOffset;

    private MovementController movementController;

    private CaterpillarController caterpillarController;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        movementController = gameplay.GetController<MovementController>();
        caterpillarController = gameplay.GetController<CaterpillarController>();
    }

    public override void LevelLoaded()
    {
        var position = movementController.cameraOrientationPoint.position +
            Quaternion.LookRotation(movementController.cameraOrientationPoint.forward, movementController.cameraOrientationPoint.up) * movePosOffset;

        var lookDirection = (movementController.cameraOrientationPoint.position + moveLookOffset) - position;

        var rotation = Quaternion.LookRotation(lookDirection);

        container.position = position;
        container.rotation = rotation;

        currentPositionOffset = movePosOffset;
        currentLookAtOffset = moveLookOffset;
    }

    void LateUpdate()
    {
        var position = movementController.cameraOrientationPoint.position +
            Quaternion.LookRotation(movementController.cameraOrientationPoint.forward, movementController.cameraOrientationPoint.up) * currentPositionOffset;

        var lookDirection = (movementController.cameraOrientationPoint.position + currentLookAtOffset) - position;

        var rotation = Quaternion.LookRotation(lookDirection);

        container.position = Vector3.Lerp(container.position, position, positionSmooth);
        container.rotation = Quaternion.Lerp(container.rotation, rotation, lookAtSmooth);

        var containerTarget = caterpillarController.caterpillar.crashState ? crashOffset : Vector3.zero;

        camera.transform.localPosition = Vector3.Slerp(camera.transform.localPosition, containerTarget, .1f);
    }

    public void SetSkinOffset()
    {
        currentPositionOffset = skinPositionOffset;
        currentLookAtOffset = skinLookOffset;
        positionSmooth = skinPosSmooth;
        lookAtSmooth = skinLookSmooth;
    }

    public void SetMoveOffset()
    {
        currentPositionOffset = movePosOffset;
        currentLookAtOffset = moveLookOffset;
        positionSmooth = movePosSmooth;
        lookAtSmooth = moveLookSmooth;
    }
}
