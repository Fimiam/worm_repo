using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkinCollider : MonoBehaviour
{
    public Segment segment;

    public new Collider collider;

    private void Start()
    {
        segment.skinCollider = this;
        collider = GetComponent<Collider>();
    }
}
