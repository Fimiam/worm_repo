using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeSkin : Segment
{
    [SerializeField] protected Bicycle bicycle;

    public Man man;

    private float stearAngle;

    protected void Update()
    {
        bicycle.UpdateGear(9);

        bicycle.UpdateWheels(9);

        man.lFootTarget.position = bicycle.leftFootTarget.position;
        man.rFootTarget.position = bicycle.rightFootTarget.position;

        man.rHandTarget.position = bicycle.ringtHandTarget.position;
        man.lHandTarget.position = bicycle.leftHandTarget.position;

        stearAngle = Mathf.Lerp(-bicycle.MaxStearAngle, bicycle.MaxStearAngle, .5f);

        bicycle.UpdateStear(stearAngle);
    }
}
