using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations.Rigging;

public class Man : MonoBehaviour
{
    public TwoBoneIKConstraint leftHandIK, rightHandIK, leftLegIK, rightLegIK;

    public Transform rHandTarget, lHandTarget, rFootTarget, lFootTarget;

    [SerializeField] private Renderer rend;

    public void SetMaterial(Material mat)
    {
        var mats = rend.materials;

        mats[1] = mat;

        rend.materials = mats;
    }
}
