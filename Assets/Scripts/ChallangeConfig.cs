﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CreateAssetMenu(fileName = "challange", menuName = "GameConfigs/challange")]
public class ChallangeConfig : ScriptableObject
{
    public ChallangeType Type;
    public int CompleteValue;
    public ChallangeRewardType RewardType;
    public int RewardValue;
    public ChallangeDiffycalty Difficulty;
    public string Description;
}

public enum ChallangeType
{
    DaysPlayed,
    SoftCollector,
    SpeedBoostersCollector,
    KeysCollector,
    RoadJumper,
    RoadFaller,
    Racer,
    Winner
}

public enum ChallangeRewardType
{
    Soft,
    Key,
    Hard
}

public enum ChallangeDiffycalty
{
    Easy,
    Medium,
    Hard
}
