using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitCollider : MonoBehaviour
{
    public Obstacle obstacle;

    private void OnCollisionEnter(Collision collision)
    {
        var segment = collision.collider.GetComponent<SkinCollider>();

        if(segment)
        {
            obstacle.HitSegment(segment);
        }
    }
}
