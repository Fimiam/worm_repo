using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CircularSaw : Obstacle
{
    [SerializeField] private Transform model;

    [SerializeField] private Vector3 rotation;

    [SerializeField] private new Collider collider;

    [SerializeField] private float acceleration = .3f, decceleration = .2f;

    [SerializeField] private ParticleSystem sparks;

    [SerializeField] private LayerMask sparksHitTouch;

    [SerializeField] private float sparksHitDistance;

    public bool sawActive;

    private float mult;

    void Start()
    {
        
    }

    void Update()
    {
        collider.enabled = sawActive;

        mult = Mathf.MoveTowards(mult, sawActive ? 1.0f : 0, (sawActive ? acceleration : decceleration) * Time.deltaTime);

        model.Rotate(mult * rotation * Time.deltaTime);

        var ray = new Ray(model.position, Vector3.down);

        if (Physics.Raycast(ray, out RaycastHit hit, 99, sparksHitTouch))
        {
            if(hit.distance < sparksHitDistance)
            {
                sparks.transform.position = hit.point;

                sparks.Play();
            }
            else
            {
                sparks.Stop();
            }
        }
        else
        {
            sparks.Stop();
        }
    }

    public override void HitSegment(SkinCollider skinCollider)
    {
        skinCollider.segment.caterpillar.CutFromSegment(skinCollider.segment);
    }
}
