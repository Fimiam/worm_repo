using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gilliotine : Obstacle
{
    [SerializeField] private Transform blade;

    [SerializeField] private AnimationCurve bladeMoveUpCurve, bladeMoveDownCurve;

    [SerializeField] private List<ParticleSystem> hitParts;

    private float moveTime;

    private bool moveDown = false;

    private float maxMoveTime;

    private void Start()
    {
        maxMoveTime = bladeMoveUpCurve.keys[bladeMoveUpCurve.keys.Length - 1].time;
    }

    private void Update()
    {
        if(moveDown)
        {
            moveTime += Time.deltaTime;

            blade.localPosition = Vector3.up * bladeMoveDownCurve.Evaluate(moveTime);

            if (moveTime > maxMoveTime)
            {
                moveDown = !moveDown;

                maxMoveTime = bladeMoveUpCurve.keys[bladeMoveUpCurve.keys.Length - 1].time;

                moveTime = 0.0f;

                hitParts.ForEach(p => p.Play());
            }
        }
        else
        {
            moveTime += Time.deltaTime;

            blade.localPosition = Vector3.up * bladeMoveUpCurve.Evaluate(moveTime);

            if(moveTime > maxMoveTime)
            {
                moveDown = !moveDown;

                maxMoveTime = bladeMoveDownCurve.keys[bladeMoveDownCurve.keys.Length - 1].time;

                moveTime = 0.0f;
            }
        }
    }

    public override void HitSegment(SkinCollider skinCollider)
    {
        skinCollider.segment.caterpillar.CutFromSegment(skinCollider.segment);
    }
}
