using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSkin : MonoBehaviour
{
    [SerializeField] private ParticleSystem eatenParts;

    [SerializeField] private Vector3 rotationVector;

    [SerializeField] private AnimationCurve floatingCurve;

    public virtual void FoodEaten()
    {
        if(eatenParts != null)
        {
            eatenParts.transform.SetParent(null);
            eatenParts.transform.localScale = Vector3.one;
            eatenParts.Play();
            Destroy(eatenParts.gameObject, 3);
        }

        gameObject.SetActive(false);
    }

    private void Start()
    {
        if (rotationVector.sqrMagnitude > 0.0f) StartCoroutine(Rotation());

        if (floatingCurve != null) StartCoroutine(Floating()); 
    }

    private IEnumerator Floating()
    {
        float time = Random.value * 30;

        while(true)
        {
            time += Time.deltaTime;

            transform.localPosition = Vector3.up * floatingCurve.Evaluate(time);

            yield return null;
        }
    }

    private IEnumerator Rotation()
    {
        while (true)
        {
            transform.Rotate(rotationVector * Time.deltaTime);

            yield return null;
        }
    }
}
