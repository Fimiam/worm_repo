using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SkinsController : GameplayController
{
    [SerializeField] private Button skinsButton, closeSkinsButton;

    [SerializeField] private CameraController cameraMovement;

    [SerializeField] private SkinsPanel skinsPanel;

    private CaterpillarController caterpillarController;

    public override void Setup(Gameplay gameplay)
    {
        base.Setup(gameplay);

        caterpillarController = gameplay.GetController<CaterpillarController>();
    }

    private void Start()
    {
        //skinsButton.onClick.AddListener(ClickOpen);
        //closeSkinsButton.onClick.AddListener(ClickClose);

        Game.Instance.ProgressData.OnSkinChanged += SkinChanged;
    }

#if UNITY_EDITOR

    private void Update()
    {
        if(Input.GetKey(KeyCode.S))
        {
            if(Input.GetKeyDown(KeyCode.RightArrow))
            {
                int skin = Game.Instance.ProgressData.CurrentSkin;

                skin++;

                var skins = Game.Instance.Configurations.skinConfigs.skins;

                if (skin > skins.Count - 1) skin = 0;

                Game.Instance.ProgressData.SetSkin(skin);
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
            {
                int skin = Game.Instance.ProgressData.CurrentSkin;

                skin--;

                var skins = Game.Instance.Configurations.skinConfigs.skins;

                if (skin < 0) skin = skins.Count - 1;

                Game.Instance.ProgressData.SetSkin(skin);
            }
        }
    }

#endif

    private void SkinChanged(int id)
    {
        foreach (var item in FindObjectsOfType<Food>())
        {
            item.SetSkin(Game.Instance.Configurations.skinConfigs.skins[id].foodSkin);
        }

        caterpillarController.caterpillar.SetSkin(id);
    }

    private void ClickClose()
    {
        skinsPanel.Hide();
        cameraMovement.SetMoveOffset();
    }

    private void ClickOpen()
    {
        skinsPanel.Show();
        cameraMovement.SetSkinOffset();
    }

    private void OnDestroy()
    {
        Game.Instance.ProgressData.OnSkinChanged -= SkinChanged;
    }
}
